import unittest
from phonebook import PhoneBook


class PhoneBookTest(unittest.TestCase):
    """
    The test cases must be concret and scope single things to test
    If an error is thrown, the rest of the test wont be executed, so there will be functionalities not tested
    """

    """
    Every test case consist on three parts:
        - Arrange: set up the object to be tested and collaborators
        - Act: exercise the unit under test
        - Assert: make claims about what happened
    Always in this order
    """

    def setUp(self) -> None:
        self.phone_book = PhoneBook()

    def tearDown(self) -> None:
        # function to cleanup everything created during the tests in the setUp. i.e.: files or whatever
        pass

    def test_phonebook_adds_names_and_numbers(self):
        # Arrange
        # Act
        self.phone_book.add("Nacho", "912345678")
        names = self.phone_book.get_names()
        numbers = self.phone_book.get_numbers()
        # Assert
        self.assertIn("Nacho", names)
        self.assertIn("912345678", numbers)

    def test_lookup_by_existing_name(self):
        # Arrange
        self.phone_book.add("Bob", "12345")
        # Act
        number = self.phone_book.lookup("Bob")
        # Assert
        self.assertEqual("12345", number)

    def test_lookup_by_non_existing_name(self):
        # Arrange
        # Act
        number = self.phone_book.lookup("Alice")
        # Assert
        self.assertEqual(None, number)

    # @unittest.skip("WIP")
    def test_empty_phonebook_is_consistent(self):
        # Arrange
        # Act
        is_consistent = self.phone_book.is_consistent()
        # Assert
        self.assertTrue(is_consistent)

    def test_is_consistent_with_different_entries(self):
        # Arrange
        self.phone_book.add("Bob", "12345")
        self.phone_book.add("Alice", "012345")
        # Act
        is_consistent = self.phone_book.is_consistent()
        # Assert
        self.assertTrue(is_consistent)

    def test_inconsistent_with_duplicate_entries(self):
        # Arrange
        self.phone_book.add("Bob", "12345")
        self.phone_book.add("Sue", "12345")
        # Act
        is_consistent = self.phone_book.is_consistent()
        # Assert
        self.assertFalse(is_consistent)

    def test_inconsistent_with_duplicate_prefix(self):
        # Arrange
        self.phone_book.add("Bob", "12345")
        self.phone_book.add("Ana", "123")
        # Act
        is_consistent = self.phone_book.is_consistent()
        # Assert
        self.assertFalse(is_consistent)
