class PhoneBook:
    def __init__(self):
        self.numbers = dict()

    def add(self, name, number):
        if name not in self.numbers:
            self.numbers[name] = number

    def lookup(self, name):
        return self.numbers.get(name)

    def is_consistent(self):
        all_numbers = list(self.numbers.values())
        for number in all_numbers:
            if all_numbers.count(number) > 1:  # repeated number
                return False
            all_numbers_copy = all_numbers.copy()
            all_numbers_copy.remove(number)  # rest of the numbers to check prefixes
            for number2 in all_numbers_copy:
                is_prefix_of_another = str(number2).startswith(str(number))
                another_is_prefix = str(number).startswith(str(number2))
                if is_prefix_of_another or another_is_prefix:
                    return False
        return True

    def get_names(self):
        return list(self.numbers.keys())

    def get_numbers(self):
        return list(self.numbers.values())
